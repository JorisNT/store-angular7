import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'shared/shared.module';

import { ModeComponent } from './components/navbar/mode/mode.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { CategoryFilterComponent } from './components/products/category-filter/category-filter.component';
import { ProductsComponent } from './components/products/products.component';

@NgModule({
  declarations: [
    NavbarComponent,
    ModeComponent,
    ProductsComponent,
    CategoryFilterComponent,
  ],
  imports: [
    SharedModule,
    RouterModule.forChild([])
  ],
  exports: [
    NavbarComponent,
    ModeComponent,
    CategoryFilterComponent
  ]
})
export class CoreModule { }
