import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CustomFormsModule } from 'ng2-validation';

import { MaterialModule } from '../material.module';
import { ProductCardComponent } from './components/product-card/product-card.component';
import { CategoryService } from './services/category.service';
import { ProductService } from './services/product.service';


@NgModule({
  declarations: [
    ProductCardComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    CustomFormsModule,
  ],
  exports: [
    ProductCardComponent,

    CommonModule,
    MaterialModule,
    CustomFormsModule
  ],
  providers: [
    ProductService,
    CategoryService
  ]
})
export class SharedModule { }
