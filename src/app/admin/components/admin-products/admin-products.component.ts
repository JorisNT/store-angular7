import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { ProductService } from 'shared/services/product.service';
import { Product } from 'shared/models/product';


@Component({
  selector: 'app-admin-products',
  templateUrl: './admin-products.component.html',
  styleUrls: ['./admin-products.component.css']
})
export class AdminProductsComponent implements OnInit {
  products$: Observable<Product[]>;

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.products$ = this.productService.getAll();
  }
}
