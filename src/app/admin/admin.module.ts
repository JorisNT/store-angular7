import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from 'shared/shared.module';

import { AdminProductItemComponent } from './components/admin-product-item/admin-product-item.component';
import { AdminProductsComponent } from './components/admin-products/admin-products.component';
import { ProductFormComponent } from './components/product-form/product-form.component';
import { AuthGuard } from './services/auth-guard.service';

const routes: Routes = [
  { path: 'products/new', component: ProductFormComponent, canActivate: [AuthGuard] },
  { path: 'products/:id', component: ProductFormComponent, canActivate: [AuthGuard] },
  { path: 'products', component: AdminProductsComponent, canActivate: [AuthGuard] }
];


@NgModule({
  declarations: [
    AdminProductsComponent,
    AdminProductItemComponent,
    ProductFormComponent
  ],
  imports: [
    FormsModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    AuthGuard
  ]
})
export class AdminModule { }
