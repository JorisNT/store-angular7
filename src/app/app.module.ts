import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';

import { CoreModule } from 'core/core.module';
import { SharedModule } from 'shared/shared.module';

import { AppComponent } from './app.component';
import { ProductsComponent } from './core/components/products/products.component';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    SharedModule,
    CoreModule,
    BrowserAnimationsModule,
    RouterModule.forRoot([
      { path: '', component: ProductsComponent },
      { path: 'admin', loadChildren: 'app/admin/admin.module#AdminModule' }
    ])
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
